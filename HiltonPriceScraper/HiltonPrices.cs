﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using HiltonPriceScraper.DTOs;
using HiltonPriceScraper.Exceptions;
using Newtonsoft.Json;

namespace HiltonPriceScraper
{
    public class HiltonPrices
    {
        private CookieContainer _cookieContainer = new CookieContainer();
        private HttpClientHandler _handler { get; set; }

        public HiltonPrices()
        {
            _handler = new HttpClientHandler()
            {
                CookieContainer = _cookieContainer,
                UseCookies = true
            };
        }

        /// <summary>
        /// Search for available hotels in a given location for your arrival and departure dates.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="arrivalDate">The arrival date, in MM/DD/YYYY format</param>
        /// <param name="departureDate">The departure date, in MM/DD/YYYY format</param>
        /// <returns></returns>
        private async Task<ExecuteSearchHotelsResult> ExecuteHotelSearch(string location, string arrivalDate,
            string departureDate)
        {
            using (var client = CreateHttpClient())
            {

                var uriBuilder = new UriBuilder()
                {
                    Host = "m.hilton.com",
                    Path = "/mt/www3.hilton.com/api/HotelSearch/searchHotels.json",
                    Query = BuildQueryString(new NameValueCollection()
                    {
                        {"faker", location},
                        {"searchQuery", location},
                        {"arrivalDate", arrivalDate},
                        {"departureDate", departureDate},
                        {"numberOfAdults[0]", "0"},
                        {"numberOfChildren[0]", "0"},
                        {"numberOfRooms", "1"},
                        {"promoCode", ""},
                        {"groupCode", ""},
                        {"corporateId", ""}
                    })
                };

                HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                var json = JsonConvert.DeserializeObject<ExecuteSearchHotelsResult>(content);

                if (json.Success != true)
                {
                    throw new HiltonPricesException(
                        String.Format("Error while searching for available hotels. Response: {0}",
                            content));
                }

                // this is really fucked up, but it's apparently the only way to tell whether the search was successful
                if (json.Redirect != "/mt/www3.hilton.com/en_US/hi/search/findhotels/results.htm?view=LIST")
                {
                    throw new HiltonPricesException("No hotels found. Likely an invalid location or date range.");
                }

                return json;

            }
        }

        /// <summary>
        /// Search for location suggestions.
        /// </summary>
        /// <param name="location"></param>
        /// <returns>A list of potential locations with a numeric ranking of likely matchability.</returns>
        public async Task<List<LocationSearchResult>> SearchLocations(string location)
        {
            using (var client = CreateHttpClient("http://m.hilton.com"))
            {

                var uriBuilder = new UriBuilder()
                {
                    Host = "m.hilton.com",
                    Path = "/mt/www3.hilton.com/api/HotelSearch/locationSuggestions.json",
                    Query = BuildQueryString(new NameValueCollection()
                    {
                        {"term", location}
                    })
                };

                HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                var json = JsonConvert.DeserializeObject<List<LocationSearchResult>>(content);

                return json;
            }
        }

        public async Task<List<HotelSearchResult>> SearchHotels(string location, string arrivalDate, string departureDate)
        {
            // actually execute the search, which returns a URL to redirect to
            var searchResult = await ExecuteHotelSearch(location, arrivalDate, departureDate);

            using (var client = CreateHttpClient("http://m.hilton.com"))
            {

                HttpResponseMessage response = await client.GetAsync(searchResult.Redirect);
                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                var regex = new Regex(@"\s*viewData = (.*)");

                if (!regex.IsMatch(content))
                {
                    throw new HiltonPricesException("Error parsing hotel search results.");
                }

                var match = regex.Match(content);

                // trim the trailing comma that we couldn't get rid of in regex for some reason
                var resultsContent = match.Groups[1].Value.TrimEnd(',');

                // when there is no messaging for a property they return an empty array rather than a messaging object. fix that stupid inconsistency
                resultsContent = resultsContent.Replace("\"messaging\":[]", "\"messaging\":null");

                // they also switch between a string and a boolean on the original price
                resultsContent = resultsContent.Replace("\"original\":false", "\"original\":null");

                // they do the same thing for special codes
                resultsContent = resultsContent.Replace("\"specialCode\":false", "\"specialCode\":null");

                var json = JsonConvert.DeserializeObject<List<HotelSearchResult>>(resultsContent);

                return json;

            }
        }

        public async Task<string> GetHotelRooms(string hotelCode)
        {
            using (var client = CreateHttpClient())
            {
                var uriBuilder = new UriBuilder()
                {
                    Host = "m.hilton.com",
                    Path = "/mt/www3.hilton.com/en_US/hi/reservation/book.htm",
                    Query = BuildQueryString(new NameValueCollection()
                    {
                        {"inputModule", "HOTEL_SEARCH"},
                        {"ctyhocn", hotelCode},
                        {"internalDeepLinking", "true"}
                    })
                };

                client.DefaultRequestHeaders.Add("Referer",
                    "http://m.hilton.com/mt/www3.hilton.com/en_US/hi/search/findhotels/results.htm?view=LIST");


                HttpResponseMessage response = await client.GetAsync(uriBuilder.Uri);
                //response.EnsureSuccessStatusCode();

                if (response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.MovedPermanently ||
                    response.StatusCode == HttpStatusCode.TemporaryRedirect)
                {
                    response = await client.GetAsync(response.Headers.Location);
                    response.EnsureSuccessStatusCode();
                }

                var content = await response.Content.ReadAsStringAsync();

                if (content.Contains("Technical Application Error</title>"))
                {
                    throw new HiltonPricesException("Hilton Application Error");
                }

                if (content.Contains("There are no rooms available for"))
                {
                    throw new NoRoomsAvailableException(
                        "No available rooms were found at the requested location for the selected dates.");
                }

                var regex = new Regex(@"roomData = (.*)");

                if (regex.IsMatch(content))
                {
                    var match = regex.Match(content);



                }

                throw new HiltonPricesException("Unable to extract room data from search results.");
            }
        }

        private static string BuildQueryString(NameValueCollection pairs)
        {
            var kvPairs = new List<string>();
            pairs.AllKeys.ToList()
                .ForEach(
                    key =>
                        kvPairs.Add(String.Format("{0}={1}", HttpUtility.UrlEncode(key),
                            HttpUtility.UrlEncode(pairs.Get(key)))));

            return String.Join("&", kvPairs);
        }

        /// <summary>
        /// Create a new instance of HttpClient using our internal cookie container.
        /// </summary>
        /// <param name="baseUrl">An optional URL to specify as the BaseAddress.</param>
        /// <returns></returns>
        private HttpClient CreateHttpClient(string baseUrl = null, bool allowAutoRedirect = true)
        {
            
            //var client = new HttpClient(new HttpClientHandler() {CookieContainer = _cookieContainer, UseCookies = true, AllowAutoRedirect = allowAutoRedirect}, false);
            var client = new HttpClient(_handler, false);
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36 OPR/32.0.1948.19 (Edition beta)");

            if (!String.IsNullOrEmpty(baseUrl))
            {
                client.BaseAddress = new Uri(baseUrl);
            }

            return client;
        }
    }
}
