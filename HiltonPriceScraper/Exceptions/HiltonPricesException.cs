﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HiltonPriceScraper.Exceptions
{
    public class HiltonPricesException : Exception
    {
        public HiltonPricesException() { }

        public HiltonPricesException(string message) : base(message)
        {
        }

        public HiltonPricesException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public HiltonPricesException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
