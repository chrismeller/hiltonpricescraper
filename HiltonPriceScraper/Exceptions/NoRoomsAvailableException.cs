﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HiltonPriceScraper.Exceptions
{
    public class NoRoomsAvailableException : HiltonPricesException
    {
        public NoRoomsAvailableException() { }

        public NoRoomsAvailableException(string message) : base(message)
        {
        }

        public NoRoomsAvailableException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NoRoomsAvailableException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
