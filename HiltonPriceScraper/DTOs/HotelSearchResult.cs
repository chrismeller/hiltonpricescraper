﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HiltonPriceScraper.DTOs
{

    public class HotelSearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string BrandLogo { get; set; }
        public bool NoRooms { get; set; }
        public bool ComingSoon { get; set; }
        public string Available { get; set; }
        public bool Confidential { get; set; }
        public string Original { get; set; }
        public bool Hhonors { get; set; }
        public bool Diamond { get; set; }
        public string InfoLink { get; set; }
        public string BookingLink { get; set; }
        public string Quicklook { get; set; }
        public string Description { get; set; }
        public string Rating { get; set; }
        public string distance { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public Picture[] pictures { get; set; }
        public string address { get; set; }
        public string currency { get; set; }
        public string SpecialCode { get; set; }
        public bool callToBook { get; set; }
        public bool brandMessage { get; set; }
        public Messaging messaging { get; set; }
        public bool digitalkey { get; set; }
    }

    public class Messaging
    {
        public string type { get; set; }
        public string text { get; set; }
    }

    public class Picture
    {
        public string path { get; set; }
        public string alt { get; set; }
        public string title { get; set; }
        public string caption { get; set; }
    }

}
