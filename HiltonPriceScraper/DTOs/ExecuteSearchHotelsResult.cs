﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiltonPriceScraper.DTOs
{
    public class ExecuteSearchHotelsResult
    {
        public bool Success { get; set; }
        public string Redirect { get; set; }
    }
}
