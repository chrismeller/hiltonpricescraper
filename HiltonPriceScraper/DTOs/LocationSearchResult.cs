﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiltonPriceScraper.DTOs
{
    public class LocationSearchResult
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public int Rank { get; set; }
    }
}
