﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiltonPriceScraper.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Search().Wait();
        }

        private static async Task Search()
        {
            var prices = new HiltonPrices();
            //await prices.SearchLocations("denv");
            await prices.SearchHotels("Greenville, SC", DateTime.Now.AddDays(365).ToString(@"MM/dd/yyyy"), DateTime.Now.AddDays(366).ToString(@"MM/dd/yyyy"));
            await prices.GetHotelRooms("GSPSCHF");

        }
    }
}
